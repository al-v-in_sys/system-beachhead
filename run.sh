#!/bin/bash
set -e

NOW=$(date +%s)

if [ ! -d ~/.ssh ]; then
    mkdir ~/.ssh
fi

if [ -f ~/.ssh/authorized_keys ]; then
    cp ~/.ssh/authorized_keys ~/.ssh/authorized_keys.$NOW
fi

## Copy SSH public key  ##
if [ -f id_rsa.pub ]; then
    echo "Copying id_rsa.pub file"
    cp id_rsa.pub ~/.ssh/authorized_keys
    cp id_rsa.pub ~/.ssh/id_rsa.pub

    chmod u=rw,go=r ~/.ssh/authorized_keys ~/.ssh/id_rsa.pub
else
    echo "id_rsa.pub file not found"
    echo
fi

sudo apt update

sudo apt install ansible git openssh-server

git config --global user.email "al-v-in@users.noreply.github.com"
git config --global user.name "Alvin B"

ansible-galaxy collection install community.general

# disable password auth (just in case we have any weak passwords)
sed -n 'H;${x;s/\#PasswordAuthentication yes/PasswordAuthentication no/;p;}' /etc/ssh/sshd_config > tmp_sshd_config
sudo mv tmp_sshd_config /etc/ssh/sshd_config

sudo service ssh restart

# show you IP so you know where to connect to
ip addr
