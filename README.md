# Alvin's System Beachhead

Small script to setup a newly created Debian based system with everything required for Alvin (his SSH key) to to manage the system over SSH and Ansible.

Installs SSH keys. Ensures SSH files and folder have correct permissions

Installs:
- ansible
- git
- openssh-server (& disables password auth)

## Instructions

1. download repo (either install git manually and clone, or download manually)
1. run `run.sh` script